<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route group: a groupon belüli összes routra igaz lesz.
//Groupon belül minden Controllert a Controllers/Admin-namespace alatt fog keresni.


use App\Http\Controllers\Admin\NotesController;

Route::middleware('customer_auth')->group(function () {
  Route::get('/ugyfelek', 'CustomersController@index')->name('customers.index');
  Route::delete('/kilepes', 'CustomerAuthController@destroy')->name('login.destroy');

  Route::get('jegyzeteim', 'NotesController@ownNotes')->name('notes.ownNotes');

  Route::delete('jegyzeteim/torles/{noteId}', 'NotesController@destroy')->name('notes.destroy');

  Route::get('/jegyzetek/uj-jegyzet', 'NotesController@create')->name('notes.create');
  Route::post('/jegyzetek/uj-jegyzet', 'NotesController@store')->name('notes.store');

  Route::get('/jegyzetek/{noteId}/modositas', 'NotesController@edit')->name('notes.edit');
  Route::put('/jegyzetek/{noteId}/modositas', 'NotesController@update')->name('notes.update');

  //a jegyzetek/ routok közül legalulra kerül, mert különben a jegyzetek/uj-jegyzet esetén az "uj-jegyzet" részt {customerId}nak gondolva ebbe futna bele.

  Route::post('/jegyzetek/{noteId}/hozzaszolas', 'CommentsController@store')->name('comments.store');
  Route::get('/stat/{type?}','StatController@index')->name('stat');
});

Route::get('/jegyzetek/{customerId?}', 'NotesController@index')->name('notes.index');
Route::get('/jegyzet/{noteId}', 'NotesController@show')->name('notes.show');

/*
Route::get('/', 'PagesController@index')->name('index');
*/
Route::get('/', function (){
    return view('frontend.auth.create');
})->name('index');

//create, store
Route::get('/regisztracio', 'CustomersController@create')->name('customers.create');
Route::post('/regisztracio', 'CustomersController@store')->name('customers.store');


//edit, update
Route::get('/ugyfel/{customerId}/modositas', 'CustomersController@edit')->name('customers.edit');
Route::put('/ugyfel/{customerId}/modositas', 'CustomersController@update')->name('customers.update');

Route::delete('ugyfel/{customerId}/torles', 'CustomersController@destroy')->name('customers.destroy');

Route::get('/ugyfel/{id}', 'CustomersController@show')->name('customers.show');


Route::get('/belepes', 'CustomerAuthController@create')->name('login.create');

Route::post('/belepes', 'CustomerAuthController@store')->name('login.store');


//  Route::post('/login', 'Auth\LoginController@login')->name('login.store');
//Route::post('/belepes', 'CustomerAuthController@store')->name('login.store');

Route::get('/customer', 'CustomersController@newCustomer')->name('customers.newCustomer');

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () { //
  //innentől pedig a Controllers/Admin/Auth-ban
  Route::namespace('Auth')->group(function () {
    Route::get('/belepes', 'LoginController@showLoginForm')->name('login.create');
    Route::get('/kilepes', 'LoginController@logout')->name('logout');
    Route::post('/belepes', 'LoginController@login')->name('login.store');
  });

  Route::middleware('admin_auth')->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::prefix('customer')->name('customer.')->group(function () {
        //Route::get('/ajaxData', 'CustomerController@ajaxData')->name('ajaxData');
        Route::resource('','CustomerController')->parameters([''=>'id']);
    });
    Route::resource('note','NotesController');
      //Route::resource(customer','CustomerController')->parameters([''=>'id']);
  });
});
