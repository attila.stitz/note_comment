<?php

//ezt a filet be kell autoloadolni. a composer.json autoload ->files majd composer dump-autoload
if(!function_exists('orderTableHeader')) {

  function orderTableHeader($field, $tableHeaderText)
  {   //  \Route-ot azért használhatom így, mert szerepel a config/app.php aliasai közt.
    $currentRouteName = \Route::currentRouteName();
    $link = '<a href="'.route($currentRouteName, [
      'search' => request()->input('search'),
      'orderby' => $field,
      'order_dir' => request()->input('orderby') == $field &&
                     request()->input('order_dir') == 'asc' ? 'desc' : 'asc'
    ]).'">'.$tableHeaderText.'</a>';


    return $link;
  }
}


if(!function_exists('authCustomer')) {
  function authCustomer()
  {
    return auth()->guard('customer')->user();
  }
}
