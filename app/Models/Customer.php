<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

/**
 * App\Models\Customer
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone
 * @property string $password
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read mixed $notes_comment_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Note[] $notes
 * @property-read int|null $notes_count
 * @method static \Illuminate\Database\Eloquent\Builder|Customer freshRegister()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer search($data)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Customer extends Authenticatable
{
    //public $table = 'customers';


    public function lastUpdated()
    {
      return $this->updated_at->format('Y-m-d');
    }

    public function notes()
    {
        //1 a többhöz kapcsolat
      return $this->hasMany(Note::class);
    }

    public function comments()
    {
      return $this->hasMany(Comment::Class);
    }

    public function getNotesCommentCountAttribute()
    {
        $notes=Note::where('customer_id',$this->id)->withCount('comments')->get();
        $sum=0;
        foreach ($notes as $note){
            $sum+=$note->comments_count;
        }
        return $sum;
    }


    public function setAttributes($data)
    {
      $this->name = $data['name'];
      $this->email = $data['email'];

      if(isset($data['password']) && $data['password']) {
        $this->password = \Hash::make($data['password']);
      }
    }

    public function scopeSearch($query, $data)
    {
      if(isset($data['name']) && $data['name']) {   //$search['name']  = Horvath;
        $query->where('name', 'LIKE', '%'.$data['name'].'%');
      }

      if(isset($data['email']) && $data['email']) {   //$search['name']  = Horvath;
        $query->where('email', 'LIKE', '%'.$data['email'].'%');
      }

      if(isset($data['orderby']) && $data['orderby']) {
          $query->orderBy($data['orderby'], $data['order_dir']);
      }
    }

    public function scopeFreshRegister($query)
    {
      //a héten regisztráltak
      $date = Carbon::now()->subWeek()->format('Y-m-d');
      $query->where('created_at', '>', $date);
    }
}
