<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * App\Models\Note
 *
 * @property int $id
 * @property int|null $customer_id
 * @property string|null $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $public_at
 * @property string|null $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\Models\Customer|null $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|Note newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Note newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Note onFrontend()
 * @method static \Illuminate\Database\Eloquent\Builder|Note query()
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note wherePublicAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Note extends Model
{

  public function getCustomerName()
  {
      $customer = Customer::find($this->customer_id);

      return $customer->name;
  }

  public function hasTag($tagId)
  {
    return $this->tags()->find($tagId);
  }

  public function customer()
  { //1-többhöz kapcsolat, 'customer_id'  idegen kulcsal.
  //  return $this->belongsTo(Customer::class/*, 'customer_id'*/);/*Default elnevezésnél nem kell megadni*/
    return $this->belongsTo(Customer::class);
  }

  public function tags()
  {
    return $this->belongsToMany(Tag::class)->withTimestamps();
  }

  public function comments()
  {
    return $this->hasMany(Comment::Class);
  }

  public function setAttributes($data)
  {
    $this->content = isset($data['content']) ? $data['content'] : '';
    $this->public_at =  isset($data['public_at']) ? $data['public_at'] : null;
    $this->title =  isset($data['title']) ? $data['title'] : null;
  }

  public function scopeOnFrontend($query)
  {
    $query->whereNotNull('public_at')->whereDate('public_at', '<=', now());
  }


}
