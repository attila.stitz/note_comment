<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerAuthController extends Controller
{
  public function create()
  {
    return view('frontend.auth.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'email' => 'required|email',
      'password' => 'required'
    ]);

    $credentials = $request->only('email', 'password');

    \Auth::guard('customer')->attempt($credentials);

    $customer = \Auth::guard('customer')->user(); //a belépett customer

    return redirect()->route('customers.index');
  }

  public function destroy()
  {
    \Auth::guard('customer')->logout();

    return redirect()->route('index');
  }
}
