<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function show($customerId)
    {
        $customer = Customer::find($customerId);

        return view('frontend.customers.show')->with('customer', $customer);
    }

    public function index(Request $request)
    {
        $search = $request->input('search'); //array
        $search['orderby'] = $request->input('orderby');
        $search['order_dir'] = $request->input('order_dir');

        $customers = Customer::search($search)->get();

        return view('frontend.customers.index')
            ->with('customers', $customers);
    }

    public function create()
    {
        $customer = new Customer;

        return view('frontend.customers.create')->with('customer', $customer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:customers,email',
            'password' => 'required|confirmed',
            'terms' => 'accepted'
        ]);

        $customer = new Customer;
        $customer->setAttributes($request->all());
        $customer->save();

        session()->flash('message', 'Köszöjük a regisztrációt');
        return redirect()->back();
    }

    public function edit($customerId)
    {
        $customer = Customer::findOrFail($customerId);

        return view('frontend.customers.edit')->with('customer', $customer);
    }

    public function update(Request $request, $customerId)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:customers,email,' . $customerId,  //unique kivéve a megadott IDra
            'password' => 'confirmed'   //nem kötelező, de ha meg van adva, a 2jelszónak egyeznie kell
        ]);

        $customer = Customer::findOrFail($customerId);
        $customer->setAttributes($request->all());

        $customer->save();

        session()->flash('message', 'Az adatok módosultak');

        return redirect()->route('customers.index');
    }

    public function destroy($customerId)
    {
        $customer = Customer::findOrFail($customerId);
        $customer->delete();
        return response()->json(['message' => 'Az ügyfél törölve']);
    }

}
