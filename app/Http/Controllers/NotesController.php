<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use App\Models\Customer;
use Carbon\Carbon;
use App\Models\Tag;

class NotesController extends Controller
{

  public function __construct()
  {
    $this->middleware('own_note')->only(['edit', 'update', 'destroy']);
  }

  public function show($noteId)
  {
    $note = Note::findOrFail($noteId);

    return view('frontend.notes.show')->with('note', $note);
  }

                        //ha nincs megadva, akkor null
  public function index($customerId = null)
  {
    if($customerId) {
        $customer = Customer::find($customerId);
        $notes = $customer->notes; //$customer->notes()->get(); rövid formája
    
        $now = Carbon::now();
        //csak mai jegyzetek.       //whereDate: elég ha a dátum egyezik, az időpont részt figyelmen kívül hagyja.
        $notes = $customer->notes()->whereDate('created_at', '=', $now)->orderBy('created_at', 'desc')->get();
    }

    $notes = Note::onFrontend()->get();

    return view('frontend.notes.index')->with('notes', $notes);
  }

  public function create()
  {                 //ha nincs megadva másik paraméter az orderBy() nál akkor növekvőbe rendez.
    $tags = Tag::orderBy('name')->get();

    return view('frontend.notes.create')->with('tags', $tags);
  }


  public function store(Request $request)
  {
      $note = new Note;
      $note->customer_id = auth()->guard('customer')->id();// ugyanaz, mint  $note->customer()->associate(auth()->guard('customer')->id())
      $note->setAttributes($request->all());
      $note->save();
      //fontos, hogy a save az attach előtt szerepeljen, mert csak a save() után, az adatbázisba kerüléskor kap a $note id-t.
      $note->tags()->attach($request->input('tags'));

      return redirect()->route('notes.ownNotes');
  }

  public function edit($noteId)
  {
    $note = authCustomer()->notes()->findOrFail($noteId);
    $tags = Tag::orderBy('name')->get();

    return view('frontend.notes.edit')
    ->with('note', $note)
    ->with('tags', $tags);
  }

  public function update(Request $request, $noteId)
  {
      $note = Note::find($noteId);
      $note->setAttributes($request->all());
      $note->save();

      $note->tags()->sync($request->input('tags'));

      return redirect()->route('notes.ownNotes');
  }

  public function ownNotes()
  {
    $notes = authCustomer()->notes;

    return view('frontend.notes.own-notes')
      ->with('notes', $notes);
  }

  public function destroy($customerId)
  {
      $customer = Customer::findOrFail($customerId);
      $customer->delete();

      return response()->json(['message' => 'Az ügyfél törölve']);
  }

}
