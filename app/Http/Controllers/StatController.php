<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Customer;
use App\Models\Note;
use App\Models\Tag;
use Illuminate\Http\Request;

class StatController extends Controller
{
    public function index(Request $request,$type=null)
    {

     if($type && method_exists($this,$type)){
         return  call_user_func([$this,$type]);
     }
     return view('frontend.stat.index');

    }

    public function LastCommenterCustomer()
    {
        //Comment::OrderBy('created_at','desc')->take(3)->get();
        $comments=Comment::OrderBy('created_at','desc')->take(3)->get(); //nem pontos mert ha ugya az irogat commentet akkor az elso haromba lehet ismetlodes
        return view('frontend.stat.'.__FUNCTION__)->with(
            'comments',$comments
        );
    }
    public function TopCommenterCustomer()
    {
        $customers=Customer::withCount('comments')->orderBy('comments_count','desc')->Orderby('name')->take(3)->get();
        return view('frontend.stat.'.__FUNCTION__)->with(
            'customers',$customers
        );
    }
    public function TopCommentedCustomer()
    {
        $usersCommentSum=[];
        foreach (Customer::all() as $customer){
            $usersCommentSum[$customer->id]=$customer->NotesCommentCount;

        }
        asort($usersCommentSum);
        $customers=[];
        for ($i = 0; $i <3 ; $i++) {
            $customers[]=Customer::find(array_key_last($usersCommentSum));
            array_pop($usersCommentSum);
        }
        return view('frontend.stat.'.__FUNCTION__)->with(
            'customers',$customers
        );
    }

    public function TopCommentedNote()
    {
        $notes=Note::withCount('comments')->orderBy('comments_count','desc')->take(3)->get();
        return view('frontend.stat.'.__FUNCTION__)->with(
            'notes',$notes
        );
    }

    public function TopCommentedTag()
    {
        //lehetne egymasba agyazot foeach-el vegig  menni, kerdes melyik a gyorsabba  db muveet, vagy a php oszyalygyartas
        $tags=Tag::selectRaw('tags.*,count(comments.id) as commentSum')
            ->leftJoin('note_tag','note_tag.tag_id','=','tags.id')
            ->leftJoin('comments','comments.note_id','=','note_tag.note_id')
            ->groupBy('tags.id')
            ->orderBy('commentSum','desc')
            ->take(3)
            ->get();

        return view('frontend.stat.'.__FUNCTION__)->with(
            'tags',$tags
        );

    }

    public function davidHaziMegoldasa()
    {
        //Legutóbb kommentelő  3 ügyfél
        $lastCommentsArray = Comment::orderBy('created_at', 'desc')->pluck('customer_id');
        $lastCommentsArray = array_unique($lastCommentsArray);
        $firstThree = array_slice($lastCommentsArray, 0, 3);
        $last3Commenters = Customer::whereIn('id', $firstThree)->get();


        //A legtöbbet kommentelő
        //1
        $customers = Customer::has('comments')->get();
        $mostCommenter = $customers->sortBy(function($customer) {
            $customer->comments->count();
        })->first();

        //2
        $mostCommenter = Comment::selectRaw('customer_id, count(*) AS sum')
            ->groupBy('customer_id')
            ->orderBy('sum', 'desc')
            ->first();

        //3
        //comments_count-ként lesz elérhető
        $mostCommenter = Customer::withCount('comments')->orderBy('comments_count', 'desc')->first();


        //Az az ügyfél, aki a legtöbb kommentet kapta
        //1
        $customerArray = [];
        foreach(Customer::has('notes')->get() as $customerWithNote) {
            $customerCommentCount = 0;
            foreach($customerWithNote->notes as $customerNote) {
                $customerCommentCount += $customerNote->comments()->count();
            }
            $customerArray[$customerWithNote->id] = $customerCommentCount;
        }
        //sorrendezés
        $orderedArray = asort($customerArray);
        //a sorbarendezett tömb utolsó eleme.
        $mostCommentedCustomerId = array_pop($orderedArray);
        $mostCommentedCustomer = Customer::find($mostCommentedCustomerId);

        //Legtöbbet kommentelt jegyzet
        $mostCommentedNote  = Note::withCount('comments')->orderBy('comments_count', 'desc')->first();

        //legtöbbet commentelt tag
        $tagArray = [];
        foreach(Tag::has('notes')->get() as $tagWithNote) {
            $tagCommentCount = 0;
            foreach($tagWithNote->notes as $tagNote) {
                $tagCommentCount += $tagNote->comments()->count();
            }
            $tagArray[$tagWithNote->id] = $tagCommentCount;
        }
        $orderedArray = asort($tagArray);
        $mostCommentedTagId = array_pop($orderedArray);
        $mostCommentedTag = Tag::find($mostCommentedTagId);

        return view('frontend.stats');

    }
}
