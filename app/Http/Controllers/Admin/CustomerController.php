<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class CustomerController extends Controller
{

    protected $htmlBuilder;

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Customer::select(['id', 'name', 'email', 'created_at', 'updated_at']);;
            return Datatables::of($query)->make(true);
            return Datatables::of(Customer::select(['id', 'name', 'email', 'created_at', 'updated_at']))->make(true);
        }
        return view('admin.customers.index');
    }


    public function ajaxData(Request $request)
    {
        $html = $this->htmlBuilder
            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Id'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => 'Email'])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At'])
            ->addColumn(['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At']);
        return view('basic', compact('html'));
        $query = Customer::select(['id', 'name', 'email', 'created_at', 'updated_at']);;
        return datatables($query)->make(true);
    }

    public function show($id)
    {
        return "hello $id " . __FUNCTION__;
    }

    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        return view('admin.customers.editForm')->with('customer', $customer);
    }

    public function update($customerId, Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|min:5',
            'email' => 'required|email|unique:customers,email,' . $customerId,  //unique kivéve a megadott IDra
            'password' => 'confirmed'   //nem kötelező, de ha meg van adva, a 2jelszónak egyeznie kell
        ]);

        $customer = Customer::findOrFail($customerId);
        $customer->setAttributes($request->all());
        $customer->save();
        //@todo:kiszedni a passwordot a kikuldott adatbol
        return response()->json(['succes' => true, 'data' => ['customer' => $customer->toArray()]]);
    }

    public function destroy($customerId)
    {
        $customer = Customer::findOrFail($customerId);
        $customer->delete();
        return response()->json(['succes' => true]);
    }

}



