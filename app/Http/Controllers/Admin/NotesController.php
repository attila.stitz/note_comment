<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Note;
use App\Models\Tag;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd(Note::with('customer')->get()->toArray());


        if ($request->ajax()) {
            $query=Note::with('customer','tags')->select();
            return Datatables::of($query)->make(true);
        }
        //dd(Note::with('customer','tags')->get()->toArray());

        return view('admin.notes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Note::with('tags')->findOrFail($id);
        return view('admin.notes.editForm')
            ->with([
                'item'=> $item,
                'tags'=>Tag::all(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required|string|min:3',
            'content' => 'required|string|min:3',
        ]);

        $item = Note::find($id);
        $item->setAttributes($request->all());
        $item->save();

        $item->tags()->sync($request->input('tags'));
        return response()->json(['succes' => true, 'data' => ['item' => $item->toArray()]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Note::findOrFail($id);
        $item->delete();
        return response()->json(['succes' => true]);
    }
}
