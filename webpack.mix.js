const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
  'node_modules/admin-lte/plugins/jquery/jquery.js',
  'node_modules/admin-lte/plugins/bootstrap/js/bootstrap.js',
  'node_modules/admin-lte/plugins/popper/umd/popper.js',
  'node_modules/admin-lte/dist/js/adminlte.js',

     'node_modules/admin-lte/plugins/datatables/jquery.dataTables.min.js',
    // 'node_modules/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js',
    // 'node_modules/admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js',
    // 'node_modules/admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js',
    // 'node_modules/admin-lte/plugins/datatables-buttons/js/dataTables.buttons.js',
    'node_modules/datatables.net-jqui/js/dataTables.jqueryui.js',
    'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
    'node_modules/admin-lte/plugins/select2/js/select2.full.min.js',
    ], 'public/js/admin.js')
  .js('resources/js/app.js', 'public/js')
  .js('resources/js/admin_custom.js', 'public/js')
  .copy('node_modules/admin-lte/plugins/fontawesome-free/webfonts', 'public/fonts')
  .sass('resources/sass/admin.scss', 'public/css')
    .sass('resources/sass/admin-custom.scss', 'public/css')
  .sass('resources/sass/app.scss', 'public/css');

mix.browserSync({
    proxy:'http://localhost:8000/',
    files:[
        'resources/views/*.blade.php',
        'resources/views/**/*.blade.php',
        'resources/views/**/**/*.blade.php',
        'resources/**',
        'resources/sass/*.*',
        'resources/js/*',
        'resources/lang/**/*',
    ]
});
