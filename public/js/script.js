$(document).ready(function(){
  $('.del-button').click(function() {
    var token = $(this).data('token');
    var url = $(this).data('url');
    var target = $(this).data('target');
    if(url !== undefined) {
      $.ajax({
         url: url,
         type: 'DELETE',
         data: { '_token': token},
         dataType:'json',
         success: function(data){
           if(data.message) {
             $(target).remove();
             alert(data.message);
           }
         }
      //  dataType: 'mycustomtype'
      });
    }
  });
});
