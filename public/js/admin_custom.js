/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/admin_custom.js":
/*!**************************************!*\
  !*** ./resources/js/admin_custom.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

window.modalId = null;

window.modalForm = function (options) {
  var otp = {
    title: false,
    ButtonClose: true,
    buttonCancel: true,
    buttonCancelText: 'Mégse',
    buttonSave: true,
    buttonSaveText: 'Mentés',
    showHeader: true,
    showFooter: false,
    callback: null,
    callbackButtonSave: null,
    modalId: 'ModalForm',
    body: '',
    bodyAjaxUrl: false
  };
  var opts = $.extend({}, otp, options);
  window.modalId = '#' + opts.modalId;
  var modal = $('#' + opts.modalId).modal();

  if (opts.buttonCancel) {
    modal.find('.btnCancel').text(opts.buttonCancelText);
  } else {
    modal.find('.btnCancel').hide();
  }

  if (opts.buttonSave) {
    modal.find('.btnSave').text(opts.buttonSaveText);
  } else {
    modal.find('.btnSave').hide();
  }

  if (opts.title) {
    modal.find('.modal-header').show();
    modal.find('.modal-title').text(opts.title);
  }

  if (!opts.showHeader) {
    modal.find('.modal-header').hide();
  }

  if (!opts.showFooter) {
    modal.find('.modal-footer').hide();
  }

  modal.find('.modal-body').html('...');

  if (opts.body) {
    modal.find('.modal-body').html(opts.body);
  } else if (opts.bodyAjaxUrl) {
    $.ajax({
      url: opts.bodyAjaxUrl,
      success: function success(data) {
        modal.find('.modal-body').html(data);
      }
    });
  }

  modal.show();
};

window.Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: function didOpen(toast) {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  }
});

/***/ }),

/***/ 1:
/*!********************************************!*\
  !*** multi ./resources/js/admin_custom.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\www\prooktatas\note_comment\resources\js\admin_custom.js */"./resources/js/admin_custom.js");


/***/ })

/******/ });