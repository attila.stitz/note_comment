window.modalId = null;
window.modalForm = function (options) {
    let otp = {
        title: false,
        ButtonClose: true,
        buttonCancel: true,
        buttonCancelText: 'Mégse',
        buttonSave: true,
        buttonSaveText: 'Mentés',
        showHeader: true,
        showFooter: false,
        callback: null,
        callbackButtonSave: null,
        modalId: 'ModalForm',
        body: '',
        bodyAjaxUrl: false
    }
    let opts = $.extend({}, otp, options);
    window.modalId = '#' + opts.modalId;
    let modal = $('#' + opts.modalId).modal();

    if (opts.buttonCancel) {
        modal.find('.btnCancel').text(opts.buttonCancelText);
    } else {
        modal.find('.btnCancel').hide();
    }
    if (opts.buttonSave) {
        modal.find('.btnSave').text(opts.buttonSaveText);
    } else {
        modal.find('.btnSave').hide();
    }
    if (opts.title) {
        modal.find('.modal-header').show();
        modal.find('.modal-title').text(opts.title);
    }
    if (!opts.showHeader) {
        modal.find('.modal-header').hide();
    }

    if (!opts.showFooter) {
        modal.find('.modal-footer').hide();
    }
    modal.find('.modal-body').html('...');
    if (opts.body) {
        modal.find('.modal-body').html(opts.body);
    } else if (opts.bodyAjaxUrl) {
        $.ajax({
            url: opts.bodyAjaxUrl,
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        });
    }
    modal.show();
}

window.Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})
