@extends('frontend.stat.index')

@section('statContent')
    <h1>Legtöbbet kommentelt {{count($tags)}} Tag</h1>
    <table border="1px solid;">
        <thead>
        <th>customer</th>
        <th>comments count </th>
        </thead>
        <tbody>

        @foreach($tags as $tag)
            <tr>
                <td>{{$tag->name}}</td>
                <td>{{$tag->commentSum}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
