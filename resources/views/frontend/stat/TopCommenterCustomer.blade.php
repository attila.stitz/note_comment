@extends('frontend.stat.index')

@section('statContent')
    <h1>A legtöbbet kommentelő {{count($customers)}} ügyfél</h1>
    <table border="1px solid;">
        <thead>
        <th>customer</th>
        <th>comments count </th>
        </thead>
        <tbody>

        @foreach($customers as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->comments_count}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
