@extends('frontend.stat.index')

@section('statContent')
    <h1>Legutóbb kommentelő {{count($comments)}} ügyfél</h1>
    <table border="1px solid;">
        <thead>
            <th>datum</th>
            <th>customer</th>
            <th>title</th>
        </thead>
    <tbody>

    @foreach($comments as $comment)
        <tr>
            <td>{{$comment->created_at}}</td>
            <td>{{$comment->customer->name}}</td>
            <td>{{$comment->note->title}}</td>
        </tr>
    @endforeach
    </tbody>
    </table>

@endsection
