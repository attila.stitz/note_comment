@extends('frontend.stat.index')

@section('statContent')
    <h1>Legtöbbet kommentelt {{count($notes)}} jegyzet</h1>
    <table border="1px solid;">
        <thead>
            <th>datum</th>
            <th>customer</th>
            <th>title</th>
            <th>commentsum</th>
        </thead>
    <tbody>

    @foreach($notes as $note)
        <tr>
            <td>{{$note->created_at}}</td>
            <td>{{$note->customer->name}}</td>
            <td>{{$note->title}}</td>
            <td>{{$note->comments_count}}</td>
        </tr>
    @endforeach
    </tbody>
    </table>

@endsection
