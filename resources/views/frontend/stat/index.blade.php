@extends('frontend.layout.application')

@section('content')

    <p>Stat</p>
    <a href="{{route('stat','LastCommenterCustomer')}}">Legutóbb kommentelő  ügyfél </a>
    | <a href="{{route('stat','TopCommenterCustomer')}}">A legtöbbet kommentelő</a>
    | <a href="{{route('stat','TopCommentedCustomer')}}">Az az ügyfél, aki a legtöbb kommentet kapta</a>
    | <a href="{{route('stat','TopCommentedNote')}}">Legtöbbet kommentelt jegyzet</a>
    | <a href="{{route('stat','TopCommentedTag')}}">Legtöbbet kommentelt Tag</a>


    @yield('statContent')

@stop

