@extends('frontend.layout.application')

@section('content')
  <h4>Ügyfelek</h4>

  <a href="{{route('customers.create')}}">Új ügyfél létrehozása</a>
<br>
  @if(session()->has('message'))
    <h3>{{session('message')}}</h3>
  @endif

<form action="{{route('customers.index')}}" method="GET">
  <table border="1">
    <tr>
      <th>Id</th>
      <th>
        {{--{!! html kódként kerül bele !!}--}}
        {!! orderTableHeader('name', 'Név') !!}
      </th>
      <th>
        {!! orderTableHeader('email', 'Email') !!}
        </th>
      <th>Módosítás dátuma</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
    <tr>
      <td><input type="text" name="search[id]" value="{{request()->input('search.id')}}"></td>
      <td><input type="text" name="search[name]" value="{{request()->input('search.name')}}"></td>
      <td><input type="text" name="search[email]" value="{{request()->input('search.email')}}"></td>
      <td><input type="text" name="search[updated_at]" value="{{request()->input('search.updated_at')}}"></td>
      <td></td>
      <td></td>
      <td></td>
      <td>
        <input type="submit" value="Keresés">
      </td>
    </tr>

    </tr>
    @foreach($customers as $customer)
      <tr id="customer-{{$customer->id}}">
        <td>{{$customer->id}}</td>
        <td>{{$customer->name}}</td>
        <td>{{$customer->email}}</td>
        <td>{{$customer->lastUpdated()}}</td>
        <td><a href="{{route('customers.show', ['id' => $customer->id])}}">Megtekintés</a></td>
        <td><a href="{{route('customers.edit', ['customerId' => $customer->id])}}">Módosítás</a></td>
        <td>
          <a href="{{route('notes.index', $customer->id)}}">
            Jegyzetek ( {{$customer->notes()->count()}} )
          </a>
        </td>
        <td>
          <button class="del-button"
            data-token="{{csrf_token()}}"
            data-id="{{$customer->id}}"
            data-url="{{route('customers.destroy', $customer->id)}}">
            Törlés
          </button>

        </td>
      </tr>
    @endforeach
  </table>
 </form>
@endsection
