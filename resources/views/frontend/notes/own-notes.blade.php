@extends('frontend.layout.application')

@section('content')
  <h4>Saját jegyzetek</h4>

  <a href="{{route('notes.create')}}">Új jegyzet létrehozása</a>
<br>
  @if(session()->has('message'))
    <h3>{{session('message')}}</h3>
  @endif

<form action="#" method="GET">
  <table border="1">
    <tr>
      <th>Id</th>
      <th>Cím</th>
      <th>Létrehozás dátuma</th>
      <th></th>
      <th></th>
    </tr>
    @foreach($notes as $note)
      <tr id="note-{{$note->id}}">
        <td>{{$note->id}}</td>
        <td>{{$note->title}}</td>
        <td>{{$note->created_at->format('Y-m-d H:i:s')}}</td>
        <td><a href="{{route('notes.edit', $note->id)}}">Módosítás</a></td>
        <td>
        <button class="del-button"
            data-token="{{csrf_token()}}"
            data-target="#note-{{$note->id}}"
            data-url="{{route('notes.destroy', $note->id)}}">
            Törlés
          </button>
        </td>
      </tr>
    @endforeach
  </table>
 </form>
@endsection{{-- @stop--}}
