@extends('frontend.layout.application')

@section('content')
  <h4>Jegyzetek</h4>

  <a href="{{route('notes.create')}}">Új jegyzet létrehozása</a>
<br>
  @if(session()->has('message'))
    <h3>{{session('message')}}</h3>
  @endif

<form action="{{route('notes.index')}}" method="GET">
  <table border="1">
    <tr>
      <th>Id</th>
      <th>Ügyfél</th>
      <th>Létrehozás dátuma</th>
      <th></th>
    </tr>
    </tr>
    @foreach($notes as $note)
      <tr>
        <td>{{$note->id}}</td>
        <td>{{$note->customer->name}}</td>
        <td>{{$note->created_at->format('Y-m-d H:i:s')}}</td>
        <td><a href="{{route('notes.show', ['noteId' => $note->id])}}">Megtekintés</a></td>


      </tr>
    @endforeach
  </table>
 </form>
@endsection
