@extends('frontend.layout.application')

@section('content')

  <h4>Jegyzetek</h4>
  @if(session()->has('message'))
    <h3>{{session('message')}}</h3>
  @else
    <form action="{{route('notes.update', $note->id)}}" method="POST">
      <input type="hidden" name="_method" value="PUT">
      @csrf
      Cím
        <input tpye="text" name="title" value="{{old('title') ?: $note->title}}">
        <br>
      Jegyzet:
      <textarea name="content">{{old('content') ?: $note->content}}</textarea>
      <br><br>
      Publikálás dátuma
      <input type="text" name="public_at" value="{{old('public_at',  $note->public_at)}}">
      <br><br>
      Cimkék:
      {{--több válasz esetén tömbként küldjük tovább--}}
      <select name="tags[]" multiple>
        @foreach($tags as $tag)
          <option value="{{$tag->id}}" {{$note->hasTag($tag->id) ? 'selected' : ''}}>{{$tag->name}}</option>
        @endforeach
      </select>

      <button type="submit">Mentés</button>
    </form>
  @endif

@stop
