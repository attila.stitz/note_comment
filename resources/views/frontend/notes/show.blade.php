@extends('frontend.layout.application')

@section('content')
  <h2>{{$note->title}}</h2>
  <h4>Író: {{$note->customer->name}}</h4>
  <p>Cimkék:
  @foreach($note->tags as $tag)
    <a href="{{route('notes.index', ['search' => ['tag_id' => $tag->id]])}}">{{$tag->name}}</a>
  @endforeach
  <p>
    {{$note->content}}
  </p>

  <hr>
  @if(authCustomer())
    <form action="{{route('comments.store', $note->id)}}" method="POST">
      @csrf
      Komment:
      <textarea name="content"></textarea>
      <button type="submit">Küldés</button>
    </form>
  @else
    <p>Hozzászólás írásáshoz jelentkezzen be</p>
  @endif
  <hr>
  @foreach($note->comments()->orderBy('created_at', 'desc')->get() as $comment)
    {{$comment->customer->name}} - {{$comment->created_at->format('Y-m-d H:i:s')}}
    <p>
      {{$comment->content}}
    </p>
    <hr>
  @endforeach
@stop
