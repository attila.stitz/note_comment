<a href="{{route('index')}}">Kezdőlap</a> |
<a href="{{route('notes.index')}}">Publikált jegyzetek</a> |
{{--@if(!auth()->guard('customer')->check())--}}
{{--app/helpers.php--}}
@if(!authCustomer())
  <a href="{{route('customers.create')}}">Regisztráció</a> |
  <a href="{{route('login.create')}}">Belépés</a> |
@endif
@if(authCustomer())
    <a href="{{route('customers.index')}}">Ügyfél lista</a> |
  <a href="{{route('notes.ownNotes')}}">Saját jegyzetek</a> |
    <a href="{{route('stat')}}">Statisztika</a> |


    Belépve: {{ authCustomer()->name }}

    <form action="{{route('login.destroy')}}" method="POST">
      @csrf
      <input type="hidden" name="_method" value="DELETE">
      <input type="submit" value="Kilépés">
    </form>
@endif
