<!DOCTYPE html>
<html>

@if(isset($titleExt))
    @include('admin.layout.head',['titleExt'=>$titleExt])
@else
    @include('admin.layout.head')
@endif
@yield('body')
<script src="{{mix('/js/admin.js')}}"></script>
<script src="{{mix('/js/admin_custom.js')}}"></script>
</html>
