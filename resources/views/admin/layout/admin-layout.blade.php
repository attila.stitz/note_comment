@if(isset($titleExt))
    @include('admin.layout.layoutCore',['titleExt'=>$titleExt])
@else
    @include('admin.layout.layoutCore')
@endif

<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    @include('admin.layout.topNavbar')
    @include('admin.layout.leftSidebar')
    @yield('content')
    @include('admin.layout.footer')
    @include('admin.layout.rightControlSidebar')
</div>
<!-- ./wrapper -->

</body>
