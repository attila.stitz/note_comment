<!DOCTYPE html>
<html>
<head>
  <link href="{{mix('css/admin.css')}}" rel="stylesheet">
</head>
<body>
<h3>ADMIN</h3>
<i class="fa fa-trophy"></i>
<div class="wrapper">
@include('admin.layout.sidebar')
  <div class="content-wrapper px-4 py-2" style="min-height: 316px;">
    @yield('content')
  </div>
</div>

<script src="/js/admin.js"></script>
</body>
</html>
