<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }} @isset($titleExt){{$titleExt}}@endisset</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{mix('css/admin.css')}}" rel="stylesheet">
    <link href="{{mix('css/admin-custom.css')}}" rel="stylesheet">
</head>
