<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item menu-open">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Starter Pages
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link active">
                <i class="far fa-circle nav-icon"></i>
                <p>Active Page</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Inactive Page</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">


          <a href="/docs/3.1//javascript" class="nav-link">
            <i class="nav-icon fas fa-code"></i>
            <p>
              JavaScript
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>

          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/docs/3.1//javascript/layout.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Layout</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/push-menu.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Push Menu</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/treeview.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Treeview</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/card-widget.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Card Widget</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/card-refresh.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>CardRefresh</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/control-sidebar.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Control Sidebar</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/direct-chat.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Direct Chat</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/todo-list.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Todo List</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/toasts.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Toasts</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/sidebar-search.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Sidebar Search</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/expandable-tables.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Expandable Tables</p>
              </a>
            </li><li class="nav-item">
              <a href="/docs/3.1//javascript/iframe.html" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>IFrame</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>