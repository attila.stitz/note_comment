@extends('admin.layout.admin-layout')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalForm">
                        Launch demo modal
                    </button>
        {{--
        --}}

        <!-- Modal -->
            <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="display: none">
                            <h5 class="modal-title" >Modal title</h5>
                            <button type="button" class="close btnClose" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btnCancel" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btnSave">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table" id="dataTableBuilder">
                <thead>
                <tr>
                    <th title="Id"> Id</th>
                    <th title="Name">Name</th>
                    <th title="Email"> Email</th>
                    <th title="Created At">Created At</th>
                    <th title="Updated At"> Updated At</th>
                    <th title="action"> </th>
                    <th title="action"> </th>
                </tr>
                </thead>
            </table>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        let dTable;
        $(document).ready(function() {

            dTable=$("#dataTableBuilder").DataTable({
                language: {
                    url: '//cdn.datatables.net/plug-ins/1.10.24/i18n/Hungarian.json'
                },
                "paging": true,
                //"pagingType": "full_numbers",
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "serverSide": true,
                "processing": true,
                "ajax": "{{route('admin.note.index')}}",
                rowId: 'id',
                "columns": [
                    {
                        "data": "id",
                        "name": "id",
                        "title": "Id",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "title",
                        "name": "title",
                        "title": "Title",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "content",
                        "render":function (data,type,row){

                          return data.length>30? data.substr(0,27)+'..':data;
                        },
                        "name": "content",
                        "title": "Jegyzet",
                        "orderable": true,
                        "searchable": true
                    },

                    {
                        "data": "customer.name",
                        "name": "customer.name",
                        "title": "Customer",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "created_at",
                        "name": "created_at",
                        "title": "Created At",
                        "orderable": true,
                        "searchable": true
                    }, {
                        "data": "updated_at",
                        "name": "updated_at",
                        "title": "Updated At",
                        "orderable": true,
                        "searchable": true
                    },{
                        data: null,
                        "title": "Action",
                        className: "dt-center ",
                        defaultContent: '<a class="btn btn-xs btn-primary dt-editorBtn editor-edit"><i class="fas fa-pencil-alt"></i> Edit</a>' +
                            '<a class="btn btn-xs btn-danger dt-editorBtn editor-delete"><i class="fas fa-trash"></i> Törlés</a>' +
                            '',
                        orderable: false,
                        "searchable": false
                    },
                ]
            });

        });


        $('#dataTableBuilder').on('click', 'a.editor-edit', function (e) {
            e.preventDefault();
            let ajaxUrl="{{route('admin.note.edit',':id')}}";
            ajaxUrl=ajaxUrl.replace(':id',dTable.row( $(this).closest('tr') ).id());
            modalForm({bodyAjaxUrl:ajaxUrl});
        } );

        $('#dataTableBuilder').on('click', 'a.editor-delete', function (e) {
            e.preventDefault();
            let ajaxUrl="{{route('admin.note.destroy',':id')}}";
            ajaxUrl=ajaxUrl.replace(':id',dTable.row( $(this).closest('tr') ).id());
            $.ajax({
                url: ajaxUrl,
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                success: function (data) {
                    dTable.ajax.reload(null,false);
                    Toast.fire({
                        icon: 'info',
                        html: 'Sikeres törlés'
                    });
                },
                error:function (xhr, ajaxOptions, thrownError){
                    if(xhr.responseJSON.errors){
                        ajaxErrorFieldHandler(xhr.responseJSON.errors);
                    };
                },
            });

        });
    </script>
@endsection
