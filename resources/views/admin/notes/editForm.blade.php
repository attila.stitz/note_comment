<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Jegyzet Adatok</h3>
    </div>
    <!-- form start -->
    <form class="editorForm" action="{{route('admin.note.update',$item->id)}}" method="POST">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <div class="card-body">
            <div class="form-group">
                <label for="inputCim">Cím</label>
                <input type="text" class="form-control" id="inputCim" placeholder="Írjon Címet" name="title"
                       value="{{$item->title}}">
            </div>

            <div class="form-group">
                <label>Jegyzet</label>
                <textarea class="form-control" name="content">{{$item->content}}</textarea>
            </div>
            <div class="form-group" data-select2-id="105">
                <label>Tagek</label>
                <select class="select2 select2-hidden-accessible select2-blue select2-container--below"
                        multiple="multiple"
                        data-placeholder="Kattints a kiválasztáshoz"
                        style="width: 100%;"  tabindex="-1" aria-hidden="false"
                        name="tags[]"
                >
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
                </select>
{{--

                <span class="select2
                    select2-container
                    select2-container--default
                    select2-container--below"
                      dir="ltr" data-select2-id="8" style="width: 100%;">
                    <span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-disabled="false"><ul class="select2-selection__rendered"><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="Select a State" style="width: 769.5px;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true">

                    </span></span>
--}}


            </div>
        </div>


        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
        </div>
    </form>
    <script>

        window.callbackAction = function (data) {
            console.log('callbackAction');
            console.log(data);
        }
        window.errorCallbackAction = function (data) {
            console.log('errorCallbackAction');
            console.log(data);
        }

        window.ajaxErrorFieldHandler = function (errors, container) {
            let toastBodyHtml = '';
            $.each(errors, function (index, value) {
                toastBodyHtml += '<div>' + value + '<div>';
            });
            Toast.fire({
                icon: 'error',
                html: toastBodyHtml
            });
        }

        $('.editorForm .btnSubmit').click(function (event) {
            var form = $('.editorForm .btnSubmit').closest('form');
            var ajaxDataForm = form.serializeArray();
            //ajaxDataForm.push({name:'callback',value:'callbackAction'});
            //ajaxDataForm.push({name:'errorCallback',value:'errorCallbackAction'});
            $.ajax({
                url: form.attr('action'),
                type: 'PUT',
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                data: ajaxDataForm,
                success: function (data) {
                    $(modalId).modal('hide');
                    dTable.ajax.reload(null, false);
                    Toast.fire({
                        icon: 'success',
                        html: 'Sikeres adatmodositas'
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (xhr.responseJSON.errors) {
                        ajaxErrorFieldHandler(xhr.responseJSON.errors);
                    }
                },
            });
            event.preventDefault();
        });

        $('.select2').val({{$item->tags->pluck('id')->toJson()}}).select2()

    </script>
</div>
