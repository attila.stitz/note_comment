<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Ügyfél Adatok</h3>
    </div>
    <!-- form start -->
    <form class="editorForm" action="{{route('admin.customer.update',$customer->id)}}" method="POST">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <div class="card-body">
            <div class="form-group">
                <label for="inputName">Email cím</label>
                <input type="text" class="form-control" id="inputName" placeholder="Enter name" name="name"
                       value="{{$customer->name}}">
            </div>
            <div class="form-group">
                <label for="inputEmail1">Email cím</label>
                <input type="email" class="form-control" id="inputEmail1" placeholder="Enter email" name="email"
                       value="{{$customer->email}}">
            </div>
            <div class="form-group">
                <label for="inputPassword1">Password</label>
                <input type="password" class="form-control" id="inputPassword1" placeholder="Password" name="password">
            </div>
            <div class="form-group">
                <label for="inputPassword2">Re Password</label>
                <input type="password" class="form-control" id="inputPassword2" placeholder="Password"
                       name="password_confirmation">
            </div>
            {{--  <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="Check1">
                  <label class="form-check-label" for="Check1">Check me out</label>
              </div>--}}
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
        </div>
    </form>
    <script>

        window.callbackAction = function (data) {
            console.log('callbackAction');
            console.log(data);
        }
        window.errorCallbackAction = function (data) {
            console.log('errorCallbackAction');
            console.log(data);
        }

        window.ajaxErrorFieldHandler = function (errors, container) {
            let toastBodyHtml = '';
            $.each(errors, function (index, value) {
                toastBodyHtml += '<div>' + value + '<div>';
            });
            Toast.fire({
                icon: 'error',
                html: toastBodyHtml
            });
        }

        $('.editorForm .btnSubmit').click(function (event) {
            var form = $('.editorForm .btnSubmit').closest('form');
            var ajaxDataForm = form.serializeArray();
            //ajaxDataForm.push({name:'callback',value:'callbackAction'});
            //ajaxDataForm.push({name:'errorCallback',value:'errorCallbackAction'});
            $.ajax({
                url: form.attr('action'),
                type: 'PUT',
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                data: ajaxDataForm,
                success: function (data) {
                    $(modalId).modal('hide');
                    dTable.ajax.reload(null, false);
                    Toast.fire({
                        icon: 'success',
                        html: 'Sikeres adatmodositas'
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (xhr.responseJSON.errors) {
                        ajaxErrorFieldHandler(xhr.responseJSON.errors);
                    }
                },
            });
            event.preventDefault();
        });


    </script>
</div>
