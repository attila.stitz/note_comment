<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;
use App\Models\Note;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create('Hu_hu');
        for ($i = 0; $i < rand(20,99); $i++) {
            $comment = new \App\Models\Comment();
            $comment->customer_id= Customer::inRandomOrder()->first()->id;
            $comment->note_id= Customer::inRandomOrder()->first()->id;
            $comment->content=$faker->realText(rand(20,100));
            $comment->save();

        }
        echo ("$i comment created");

    }
}
