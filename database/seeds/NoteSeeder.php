<?php

use App\Models\Customer;
use App\Models\Note;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class NoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Note::truncate();
        $lastWeek=new Carbon('last week');
        $lastDay=new Carbon('last day');

        $faker = Faker\Factory::create('Hu_hu');
        $tagNumber=\App\Models\Tag::count();
        for ($i = 0; $i < rand(15,50); $i++) {
            $note = new Note;
            $note->content = $faker->realText(rand(50,150));
            $note->customer_id= Customer::inRandomOrder()->first()->id;
            $note->public_at=Carbon::createFromTimestamp(rand($lastWeek->timestamp,$lastDay->timestamp))->toDateTimeString();
            $note->title=$faker->realText(30,1);
            $note->save();
            $tags=[];
            echo "note#$note->id ";
            for ($ti = 0; $ti <= rand(0,$tagNumber); $ti++) {
                $tagId=\App\Models\Tag::inRandomOrder()->first()->id;
                $tags[]=$tagId;
                echo ' tags#'.$tagId;
            }
            echo PHP_EOL;
            $note->tags()->attach($tags);
        }


    }
}
