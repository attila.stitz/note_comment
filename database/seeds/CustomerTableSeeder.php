<?php

use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::truncate();
        $customer = new Customer;
        $customer->name = 'Ödön';
        $customer->email = 'odon@cim.hu';
        $customer->password = Hash::make('123456');
        $customer->save();
        $customer = new Customer;
        $customer->name = 'Ödön2';
        $customer->email = 'odonke@cim.hu';
        $customer->password = Hash::make('123456');
        $customer->save();
        factory(Customer::class, 10)->create();
    }
}
