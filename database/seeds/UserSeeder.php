<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $admin=new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@admin.hu';
        $admin->password = Hash::make('123456');
        $admin->save();

    }
}
